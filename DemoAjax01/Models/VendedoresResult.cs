﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoAjax01.Models
{
    public class VendedoresResult
    {
        public int Total { get; set; }
        public IEnumerable<tblVendedores> Data { get; set; }
        public string Message { get; set; }
    }
}