﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoAjax01.Models
{
    public class PedidosXClienteResult
    {
        public int Total { get; set; }
        public IEnumerable<spPedidosXCliente_Result> Data { get; set; }
        public string Message { get; set; }
    }
    public class PedidosXVendedorResult
    {
        public int Total { get; set; }
        public IEnumerable<spPedidosXVendedor_Result> Data { get; set; }
        public string Message { get; set; }
    }
}