﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoAjax01.Models
{
    public class ClientesResult
    {
        public int Total { get; set; }
        public IEnumerable<tblClientes> Data { get; set; }
        public string Message { get; set; }
    }
}