﻿using DemoAjax01.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DemoAjax01.Controllers
{
    public class VendedoresController : Controller
    {
        ConnectionString db = new ConnectionString();

        public JsonResult GetAll()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var data = db.tblVendedores.ToList();
            var result = new VendedoresResult { Total = data.Count, Data = data, Message = "OK" };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
	}
}