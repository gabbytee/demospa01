﻿using DemoAjax01.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DemoAjax01.Controllers
{
    public class PedidosController : Controller
    {
        ConnectionString db = new ConnectionString();

        public JsonResult GetByClientesId(string id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var data = db.spPedidosXCliente(id).ToList();
            var result = new PedidosXClienteResult { Total = data.Count, Data = data, Message = "Id:" + id };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetByVendedoresId(string id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var data = db.spPedidosXVendedor(id).ToList();
            var result = new PedidosXVendedorResult { Total = data.Count, Data = data, Message = "Id:" + id };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
	}
}