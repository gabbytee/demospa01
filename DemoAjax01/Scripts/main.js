﻿require.config({
    'baseUrl': '../Scripts',
    'paths': {
        'jquery': 'lib/jquery-1.10.2.min',
        'underscore': 'lib/underscore-min',
        'bootstrap':'lib/bootstrap-min',
    },
    'shim': {
        'bootstrap' : ['jquery']
    }
});

require(['app/app']);