﻿define(['underscore'], function (_) {

    _.templateSettings = {
        interpolate: /\{\{\=(.+?)\}\}/g,
        evaluate: /\{\{(.+?)\}\}/g,
        escape: /\{\{\-(.+?)\}\}/g
    };

    Number.prototype.formatMoney = function (decPlaces, thouSeparator, decSeparator, currencySymbol) {
        decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces;
        decSeparator = decSeparator == undefined ? "." : decSeparator;
        thouSeparator = thouSeparator == undefined ? "," : thouSeparator;
        currencySymbol = currencySymbol == undefined ? "$" : currencySymbol;
        var n = this,
            sign = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(decPlaces)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
        return sign + currencySymbol + (j ? i.substr(0, j) + thouSeparator : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thouSeparator) + (decPlaces ? decSeparator + Math.abs(n - i).toFixed(decPlaces).slice(2) : "");
    }

    return {
        parseJsonDate: function (jsonDate) {
            var offset = new Date().getTimezoneOffset() * 60000;
            var parts = /\/Date\((-?\d+)([+-]\d{2})?(\d{2})?.*/.exec(jsonDate);
            if (parts[2] == undefined)
                parts[2] = 0;
            if (parts[3] == undefined)
                parts[3] = 0;
            return new Date(+parts[1] + offset + parts[2] * 3600000 + parts[3] * 60000);
        },
        fullPath: function (el) {
            var names = [];
            while (el.parentNode) {
                if (el.id) {
                    names.unshift('#' + el.id);
                    break;
                } else {
                    if (el == el.ownerDocument.documentElement) names.unshift(el.tagName);
                    else {
                        for (var c = 1, e = el; e.previousElementSibling; e = e.previousElementSibling, c++);
                        names.unshift(el.tagName + ":nth-child(" + c + ")");
                    }
                    el = el.parentNode;
                }
            }
            return names.join(" > ");
        },
        Comparator: {
            string_comparator: function (param_name, compare_depth) {
                if (param_name[0] == '-') {
                    param_name = param_name.slice(1),
                    compare_depth = compare_depth || 10;
                    return function (item) {
                        return String.fromCharCode.apply(String,
                           _.map(item[param_name].slice(0, compare_depth).split(""), function (c) {
                               return 0xffff - c.charCodeAt();
                           })
                       );
                    };
                } else {
                    return function (item) {
                        return item[param_name];
                    };
                }
            },
            number_comparator: function (param_name) {
                if (param_name[0] == '-') {
                    return function (item) {
                        return item * -1;
                    };
                } else {
                    return function (item) {
                        return item;
                    };
                }
            }
        }

    }
});

