﻿require(['app/view'], function (view) {
    $(document).ready(function () {
        view.addEntidadHandler('Clientes');
        view.addEntidadHandler('Vendedores');
        view.addHeaderData1Handler();
        view.addHeaderData2Handler();
    });
});
