﻿define(['jquery', 'underscore', 'app/data', 'app/utils'], function ($, _, appData, utils) {

    var View = function (model, elemTemplate, args) {
        this.model = model;
        this.template = _.template($(elemTemplate).html());
        this.args = args;
        this.render = function (el) {
            $(el).html(this.template(model));
        };
    };

    var renderLoading = function (el) {
        var img = '../imgs/ajax-loader.gif';
        $(el).html("<img src='" + img + "' border='0' />")
    }

    var renderTable1 = function (data, entidad) {
        if (data.length > 0 && typeof data == 'object') {
            appData.Store.model1 = data;
            var view1 = new View(
                data,
                '#tpl_data1_table',
                { entidad: entidad }
            );
            view1.render('#data1');
            renderTable2('<p class="disabled">Haga clic en un registro para ver sus pedidos asociados</p>');
        }
        else {
            $('#data1').html(data);
            renderTable2('');
        }
    };

    var renderTable2 = function (data, entidad) {
        if (data.length > 0 && typeof data == 'object') {
            var titEntidad = '';
            if (entidad == 'Clientes') {
                titEntidad = 'Vendedor'
            }
            else if (entidad == 'Vendedores') {
                titEntidad = 'Cliente'
            }
            else {
                $('#titulo_pedidos').html('');
                $('#data2').html('Error de procesamiento');
                return;
            }
            var total = 0;
            var count = 0;
            _.each(data, function (value, key, list) {
                count++;
                total += value.decValorTotal;
                if (entidad == 'Clientes') {
                    list[key].strNombre = list[key].strNombreVendedor;
                }
                else if (entidad == 'Vendedores') {
                    list[key].strNombre = list[key].strNombreCliente;
                }
                list[key].intFecha = utils.parseJsonDate(list[key].dtmFecha).getTime();
                list[key].strFecha = utils.parseJsonDate(list[key].dtmFecha).toLocaleDateString();
                list[key].strValorTotal = list[key].decValorTotal.formatMoney(0);
            });
            appData.Store.model2 = data;
            var view2 = new View(
                data,
                '#tpl_data2_table',
                {
                    entidad: entidad,
                    titEntidad: titEntidad,
                    total: total,
                    strTotal: total.formatMoney(0),
                    count: count
                }
            );
            view2.render('#data2');
            $('#titulo_pedidos').html('Pedidos');
        }
        else if (data.length == 0 && typeof data == 'object') {
            $('#titulo_pedidos').html('');
            $('#data2').html('<p class="disabled">No tiene pedidos asociados</p>');
        }
        else {
            $('#titulo_pedidos').html('');
            $('#data2').html(data);
        }
    }

    var addEntidadHandler = function (entidad) {
        $('#' + entidad).click(function () {
            $('#titulo_entidad').html(entidad);
            renderLoading('#data1');
            appData.fetchAllEntidad(entidad)
            .done(function (response) {
                renderTable1(response.Data, entidad);
            })
            .fail(function (xhr) {
                renderTable1(xhr.responseText, '');
            });
        });

        $('#data1').on('click', '.' + entidad + 'Row', function () {
            renderLoading('#data2');
            appData.fetchPedidosByEntidad(entidad, $(this).attr('data-id'))
            .done(function (response) {
                renderTable2(response.Data, entidad);
            })
            .fail(function (xhr) {
                renderTable2(xhr.responseText, '');
            });
        });
    };

    var addHeaderData1Handler = function () {
        $('#data1').on('click', '.sortable1', function (event) {
            var elem = $(this);
            var elemSelector = utils.fullPath(elem[0]);
            var fieldName = elem.attr('data-field');
            var fieldType = elem.attr('data-type');
            var newModel1 = appData.Store.model1;
            var entidad = $('#titulo_entidad').html();
            if ($(elemSelector).hasClass('sorted')) {
                if ($(elemSelector).hasClass('sorted-descending')) {
                    newModel1 = _.sortBy(appData.Store.model1, (fieldType == 'string' ? utils.Comparator.string_comparator(fieldName) : fieldName));
                    renderTable1(newModel1, entidad);
                } else {
                    newModel1 = _.sortBy(appData.Store.model1, (fieldType == 'string' ? utils.Comparator.string_comparator(fieldName) : fieldName)).reverse();
                    renderTable1(newModel1, entidad);
                    $(elemSelector).addClass('sorted-descending');
                }
            } else {
                newModel1 = _.sortBy(appData.Store.model1, (fieldType == 'string' ? utils.Comparator.string_comparator(fieldName) : fieldName));
                renderTable1(newModel1, entidad);
            }
            $(elemSelector).addClass('sorted');
        });
    };

    var addHeaderData2Handler = function () {
        $('#data2').on('click', '.sortable2', function (event) {
            var elem = $(this);
            var elemSelector = utils.fullPath(elem[0]);
            var fieldName = elem.attr('data-field');
            var fieldType = elem.attr('data-type');
            var newModel2 = appData.Store.model2;
            var entidad = $('#titulo_entidad').html();
            if ($(elemSelector).hasClass('sorted')) {
                if ($(elemSelector).hasClass('sorted-descending')) {
                    newModel2 = _.sortBy(appData.Store.model2, (fieldType == 'string' ? utils.Comparator.string_comparator(fieldName) : fieldName));
                    renderTable2(newModel2, entidad);
                } else {
                    newModel2 = _.sortBy(appData.Store.model2, (fieldType == 'string' ? utils.Comparator.string_comparator(fieldName) : fieldName)).reverse();
                    renderTable2(newModel2, entidad);
                    $(elemSelector).addClass('sorted-descending');
                }
            } else {
                newModel2 = _.sortBy(appData.Store.model2, (fieldType == 'string' ? utils.Comparator.string_comparator(fieldName) : fieldName));
                renderTable2(newModel2, entidad);
            }
            $(elemSelector).addClass('sorted');
        });
    };

    return {
        addEntidadHandler: addEntidadHandler,
        addHeaderData1Handler: addHeaderData1Handler,
        addHeaderData2Handler: addHeaderData2Handler
    }

})



