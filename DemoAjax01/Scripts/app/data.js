﻿define(['jquery'], function ($) {

    var fetchAllEntidad = function (entidad) {
        return $.ajax({
            url: '../' + entidad + '/GetAll',
            type: 'GET',
            cache: false,
            dataType: 'json',
        });
    };

    var fetchPedidosByEntidad = function (entidad, id) {
        return $.ajax({
            url: '../Pedidos/GetBy' + entidad + 'Id',
            type: 'GET',
            cache: false,
            data: {
                id: id
            },
            dataType: 'json',
        });
    };

    return {
        Store: {},
        fetchAllEntidad: fetchAllEntidad,
        fetchPedidosByEntidad: fetchPedidosByEntidad
    };
})